﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CoveringTriangles;
using CoveringTriangles.Geometry;
using UnityEngine;
using Plane = CoveringTriangles.Geometry.Plane;

public class PlaneView : MonoBehaviour
{
    [SerializeField] private GameObject dotPrefab;
    [SerializeField] public float scale = 0.2f;
    [SerializeField] private LineRenderer lineRenderer;
    [SerializeField] private float lineWidth;
    
    public List<DotView> dots = new List<DotView>();
    public static PlaneView singleton;
    
    // Start is called before the first frame update
    void Start()
    {
        singleton = this;
        lineRenderer.startWidth = lineWidth;
        lineRenderer.endWidth = lineWidth;
//        AddDot(new Vector2(0, 0));
//        AddDot(new Vector2(10, 10));
//        AddDot(new Vector2(-3, -7));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeScale(float newScale)
    {
        scale = newScale;
        foreach (var dot in dots)
        {
            dot.transform.position = dot.originalPosition * scale;
        }
    }

    public void AddDot(Vector2 position, string identifier=null)
    {
        if (identifier == null)
        {
            identifier = AlphabeticalNameGenerator.GenerateUnique(dots.Select(d => d.identifier));
        }
        DotView dotInstance = Instantiate(dotPrefab).GetComponent<DotView>();
        dotInstance.originalPosition = position;
        dotInstance.transform.position = position * scale;
        dotInstance.SetIdentifier(identifier);
        dotInstance.transform.parent = transform;
        dots.Add(dotInstance);
    }

    public void Display(Plane plane)
    {
        Clear();
        foreach (var point in plane.Points)
        {
            AddDot(new Vector2(point.X, point.Y), point.Name);
        }
    }

    public void HighlightTriangle(Triangle triangle, Color color)
    {
        foreach (var point in triangle.Points)
        {
            var dot = dots.First(p => p.identifier == point.Name);
            dot.SetColor(color);
        }
    }
    public void HighlightTriangle(Triangle triangle)
    {
        foreach (var dot in dots)
        {
            dot.SetColor(Color.black);
        }
        List<Vector3> points = new List<Vector3>();
        foreach (var point in triangle.Points)
        {
            var dot = dots.First(p => p.identifier == point.Name);
            dot.SetColor(Color.red);
            points.Add(new Vector3(dot.transform.position.x, dot.transform.position.y, dot.transform.position.z));
        }
        points.Add(points[0]);
        lineRenderer.positionCount = points.Count;
        lineRenderer.SetPositions(points.ToArray());
    }

    public void Clear()
    {
        foreach (var dot in dots)
        {
            Destroy(dot.gameObject);
        }
        dots.Clear();
        lineRenderer.positionCount = 0;
    }
}
