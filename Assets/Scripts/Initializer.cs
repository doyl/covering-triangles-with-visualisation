﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using CoveringTriangles.Algorithms;
using CoveringTriangles.DataGeneration;
using UnityEngine;
using Plane = CoveringTriangles.Geometry.Plane;
using System.Numerics;
using CoveringTriangles;
using TMPro;
using UnityEngine.Experimental.UIElements;
using UnityEngine.UI;
using Vector2 = System.Numerics.Vector2;

public class Initializer : MonoBehaviour
{
    [SerializeField] private TMP_InputField pointsCountInputField;
    [SerializeField] private TMP_InputField iterationsInputField;
    [SerializeField] private PlaneView planeView;
    [SerializeField] private Text avarageTime;
    
    private List<IPointsGenerator> pointsGenerators = new List<IPointsGenerator>();
    private List<IMaxTriangleCoverSearcher> algorithms = new List<IMaxTriangleCoverSearcher>();

    private bool keepGenerating = false;
    
    private int choosenGenerator;
    private int choosenAlgorithm;

    private int pointsCount;
    private int iterations = 0;

    // Start is called before the first frame update
    void Start()
    {
        pointsGenerators.Add(new RandomPointsGenerator());
        pointsGenerators.Add(new RegularPolygonGenerator());
        pointsGenerators.Add(new FromPlaneViewGenerator(planeView));

        algorithms.Add(new ExpandingTrianglesCoverSearcher());
        algorithms.Add(new BruteForceWithTableSearcher());
        algorithms.Add(new BruteForceMaximumTriangleCoverSearcher());
    }

    public void SetKeepGenerating(int keep)
    {
        keepGenerating = keep != 0;
    }

    public void SetGenerator(int index)
    {
        choosenGenerator = index;
    }

    public void SetAlgorithm(int index)
    {
        choosenAlgorithm = index;
    }

    public void SetIterations(string iterations)
    {
        if (string.IsNullOrEmpty(iterations))
        {
            //pointsCountInputField.text = "3";
            return;
        }
        int count = int.Parse(iterations);
        this.iterations = count;
    }

    public void SetPointsCount(string countString)
    {
        if (string.IsNullOrEmpty(countString))
        {
            //pointsCountInputField.text = "3";
            return;
        }
        int count = int.Parse(countString);
        pointsCount = count;
    }

    public void RunGeneration()
    {
        if (pointsCount < 3)
            pointsCount = 3;
        pointsCountInputField.text = pointsCount.ToString();
        Program.Generate(pointsGenerators[choosenGenerator], pointsCount);
    }

    public void RunCalculations()
    {
        if (iterations < 1)
            iterations = 1;
        iterationsInputField.text = iterations.ToString();
        TimeSpan avarage;
        if (keepGenerating)
        {
            avarage = Program.Calculate(algorithms[choosenAlgorithm], iterations, pointsGenerators[choosenGenerator]);
        }
        else
        {
            avarage = Program.Calculate(algorithms[choosenAlgorithm], iterations, pointsGenerators[2]);
        }
        avarageTime.text = "Avarage Time: " + avarage;
    }
}
