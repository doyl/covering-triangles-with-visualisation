﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsAdder : MonoBehaviour
{
    [SerializeField] private PlaneView planeView;
    private bool isActive = false;

    public void ReactToDropdownChange(int dropdown)
    {
        isActive = dropdown == 2;
    }
    
    public void AddDot(Vector2 position)
    {
        planeView.AddDot(position / planeView.scale);
    }

    public void Update()
    {
        if (isActive && Input.GetMouseButtonDown(1))
        {
            Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            AddDot(mousePosition);
        }
    }
}
