﻿using System;
using UnityEngine;
 
public class CameraDrag : MonoBehaviour
{
    public float dragSpeed = 2;
    private Vector3 dragOrigin;
    [SerializeField] private float scrollDistanceIncrement = 1;

    private bool bDragging;
    private Vector3 oldPos;
    private Vector3 panOrigin;

    private void Start()
    {
        Camera.main.transform.position = new Vector3(10, 10, -10);
        Camera.main.orthographicSize = 10;
    }

    void LateUpdate()
    {
        if(Input.GetMouseButtonDown(0))
        {
            bDragging = true;
            oldPos = transform.position;
            panOrigin = Camera.main.ScreenToViewportPoint(Input.mousePosition);                    //Get the ScreenVector the mouse clicked
        }
 
        if(Input.GetMouseButton(0))
        {
            Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition) - panOrigin;    //Get the difference between where the mouse clicked and where it moved
            transform.position = oldPos + - pos * dragSpeed;                                         //Move the position of the camera to simulate a drag, speed * 10 for screen to worldspace conversion
        }
 
        if(Input.GetMouseButtonUp(0))
        {
            bDragging = false;
        }

        if (Input.GetAxis ("Mouse ScrollWheel") > 0 && Camera.main.orthographicSize > 1) 
        {
            Camera.main.orthographicSize = Camera.main.orthographicSize -= scrollDistanceIncrement;
        }
        
        if (Input.GetAxis ("Mouse ScrollWheel") < 0) {
            Camera.main.orthographicSize = Camera.main.orthographicSize += scrollDistanceIncrement;
        }
    }
 
 
}