﻿using System;
using System.Collections;
using System.Collections.Generic;
using CoveringTriangles;
using UnityEngine;

public class DotView : MonoBehaviour
{
    [SerializeField] private SpriteRenderer renderer;
    public Vector2 originalPosition;
    public string identifier;

    public void Start()
    {
        //renderer = GetComponent<SpriteRenderer>();
        //SetColor(Color.magenta);
    }

    public void SetIdentifier(string id)
    {
        identifier = id;
    }

    public void SetColor(Color color)
    {
        renderer.color = color;
    }
}
