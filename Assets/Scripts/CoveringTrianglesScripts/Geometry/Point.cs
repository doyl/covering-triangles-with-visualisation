﻿using System.Numerics;

namespace CoveringTriangles.Geometry
{
    public class Point
    {
        public float X { get; }
        public float Y { get; }
        public string Name { get; }

        public Point(float x, float y, string name)
        {
            X = x;
            Y = y;
            Name = name;
        }

        public override string ToString()
        {
            return Name + ": " + "x: " + X + " y: " + Y;
        }

        public Vector2 ToVector2()
        {
            return new Vector2(X, Y);
        }
    }
}