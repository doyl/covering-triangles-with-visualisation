﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CoveringTriangles.Geometry
{
    public class Plane
    {
        public List<Point> Points { get; } = new List<Point>();
        public List<Triangle> PossibleTriangles { get; } = new List<Triangle>();

        public void AddPoint(float x, float y)
        {
            Points.Add(new Point(x, y, AlphabeticalNameGenerator.GenerateUnique(Points.Select(t => t.Name))));
        }

        public override string ToString()
        {
            string output = "Points: \n";
            foreach (Point point in Points)
            {
                output += (point.ToString() + '\n');
            }

            output += "Triangles: \n";
            foreach (Triangle triangle in PossibleTriangles)
            {
                output += triangle.ToString() + '\n';
            }

            return output;
        }

        public void CalculateTriangles()
        {
            PossibleTriangles.Clear();
            if (Points.Count < 3)
                return;
            
            for(int i=0; i<Points.Count-2; ++i)
                for(int j=i+1; j<Points.Count; ++j)
                for (int k = j+1; k < Points.Count; ++k)
                {
                    if(!PointsCollinearityChecker.ArePointsCollinear(Points[i], Points[j], Points[k]))
                        PossibleTriangles.Add(new Triangle(Points[i], Points[j], Points[k]));
                }
        }
    }

    internal static class PointsCollinearityChecker
    {
        public static bool ArePointsCollinear(Point p1, Point p2, Point p3)
        {
            if (p1.Y == p2.Y && p1.Y == p3.Y)
                return true;
            if (p1.Y == p2.Y && p1.Y != p3.Y)
                return false;
            if (p2.Y == p3.Y && p1.Y != p3.Y)
                return false;
            if (p1.Y == p3.Y && p2.Y != p3.Y)
                return false;
            
            float slope1 = (float)(p1.X - p2.X) / (p1.Y - p2.Y);
            float slope2 = (float)(p1.X - p3.X) / (p1.Y - p3.Y);
            float slope3 = (float)(p2.X - p3.X) / (p2.Y - p3.Y);
            
            if (Math.Abs(slope1 - slope2) < 0.0001f || Math.Abs(slope1 - slope3) < 0.0001f || Math.Abs(slope2 - slope3) < 0.0001f)
                return true;
            return false;
        }
    }
}