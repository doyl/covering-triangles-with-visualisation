﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using CoveringTriangles.Utilities;

namespace CoveringTriangles.Geometry
{
    public class Triangle
    {
        public List<Point> Points { get; } = new List<Point>();

        public Triangle(Point first, Point second, Point third)
        {
            Points.Add(first);
            Points.Add(second);
            Points.Add(third);
        }

        public bool IsCoveringPoint(Vector2 point)
        {
            Vector2 a = new Vector2(Points[0].X, Points[0].Y);
            Vector2 b = new Vector2(Points[1].X, Points[1].Y);
            Vector2 c = new Vector2(Points[2].X, Points[2].Y);
            if (PointsOnTheSameSideChecker.IsPointOnSameSide(point, a, b, c)
                && PointsOnTheSameSideChecker.IsPointOnSameSide(point, b, c, a)
                && PointsOnTheSameSideChecker.IsPointOnSameSide(point, c, b, a))
            {
                return true;
            }

            return false;
        }

        public override string ToString()
        {
            string output = "";
            foreach (Point point in Points)
            {
                output += point.Name + " ";
            }

            return output;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Triangle);
        }

        public override int GetHashCode()
        {
            return (Points != null ? Points.GetHashCode() : 0);
        }

        private bool Equals(Triangle triangle)
        {
            return triangle != null && Points.All(point => triangle.Points.Contains(point));
        }
    }
}