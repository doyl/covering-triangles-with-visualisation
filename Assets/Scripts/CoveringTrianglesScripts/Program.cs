﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Numerics;
using CoveringTriangles.Algorithms;
using CoveringTriangles.DataGeneration;
using CoveringTriangles.Geometry;
using Plane = CoveringTriangles.Geometry.Plane;

namespace CoveringTriangles
{
    static class Program
    {
        public static IPointsGenerator generator;
        public static Plane plane;
        
        public static void Generate(IPointsGenerator pointsGenerator, int pointsCount)
        {
            generator = pointsGenerator;
            plane = GetRandomPlane(pointsCount, generator);
            PlaneView.singleton.Display(plane);
        }

        public static TimeSpan Calculate(IMaxTriangleCoverSearcher algorithm, int iterations, IPointsGenerator generator)
        {
            TimeSpan medium = GetMediumTime(algorithm, plane, iterations, generator);
            PlaneView.singleton.HighlightTriangle(algorithm.Winning);
            return medium;
        }
        
        public static void Main(IPointsGenerator pointsGenerator, int pointsCount)
        {
            IPointsGenerator generator = pointsGenerator;
            Plane plane = GetRandomPlane(pointsCount, generator);
            PlaneView.singleton.Display(plane);
            IMaxTriangleCoverSearcher bruteForceSearcher = new BruteForceWithTableSearcher();
            IMaxTriangleCoverSearcher expandingTrianglesCoverSearcher = new ExpandingTrianglesCoverSearcher();

            //TimeSpan time = GetMediumTime(expandingTrianglesCoverSearcher, plane, 10, generator); 
            TimeSpan time = MeasureTimeForAlgorithm(expandingTrianglesCoverSearcher, plane);
            UnityEngine.Debug.Log("Expanding: " + expandingTrianglesCoverSearcher.Winning + ": " + expandingTrianglesCoverSearcher.WinningNumberOfCovers+ " Time: "+ time);

            //time = GetMediumTime(bruteForceSearcher, plane, 10, generator);
            time = MeasureTimeForAlgorithm(bruteForceSearcher, plane);
            UnityEngine.Debug.Log("Brute force: " + bruteForceSearcher.Winning + ": " + bruteForceSearcher.WinningNumberOfCovers + " Time: " + time);
            PlaneView.singleton.HighlightTriangle(expandingTrianglesCoverSearcher.Winning);
        }

        private static TimeSpan GetMediumTime(IMaxTriangleCoverSearcher searcher, Plane plane, int repeats, IPointsGenerator generator)
        {
            TimeSpan totalTime = TimeSpan.Zero;
            for (int i = 0; i < repeats; ++i)
            {
                plane = GetRandomPlane(plane.Points.Count, generator);
                totalTime += MeasureTimeForAlgorithm(searcher, plane);
            }
            PlaneView.singleton.Display(plane);
            return new TimeSpan(totalTime.Ticks/repeats);
        }
        private static TimeSpan MeasureTimeForAlgorithm(IMaxTriangleCoverSearcher searcher, Plane plane)
        {
            Stopwatch sw = new Stopwatch();

            sw.Start();
            searcher.SearchForPlane(plane);
            sw.Stop();

            return sw.Elapsed;
        }

        private static Plane GetRandomPlane(int pointsCount, IPointsGenerator generator)
        {
            Plane plane = new Plane();

            IEnumerable<Vector2> vectors = generator.GeneratePoints(pointsCount);
            foreach (var vec in vectors)
            {
                plane.AddPoint(vec.X, vec.Y);
            }

            return plane;
        }
    }
}