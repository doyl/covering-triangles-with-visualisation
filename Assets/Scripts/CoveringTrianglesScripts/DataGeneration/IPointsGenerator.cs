﻿using System.Collections.Generic;
using System.Numerics;

namespace CoveringTriangles.DataGeneration
{
    internal interface IPointsGenerator
    {
        IEnumerable<Vector2> GeneratePoints(int count);
    }
}