﻿using System;
using System.Collections.Generic;
using System.Numerics;

namespace CoveringTriangles.DataGeneration
{
    internal class RegularPolygonGenerator : IPointsGenerator
    {
        public IEnumerable<Vector2> GeneratePoints(int count)
        {
            float r = 40;
            Vector2 center = new Vector2(50,50);
            List<Vector2> vectors = new List<Vector2>();
            float step = 360f / count;
            float angle = 0;
            for (double i = angle; i < 360.0; i += step) //go in a full circle
            {
                vectors.Add(DegreesToXY(angle, r, center)); //code snippet from above
                angle += step;
            }

            return vectors;
        }
        
        private Vector2 DegreesToXY(float degrees, float radius, Vector2 origin)
        {
            Vector2 xy = new Vector2();
            double radians = degrees * Math.PI / 180.0;

            xy.X = (float) Math.Cos(radians) * radius + origin.X;
            xy.Y = (float) Math.Sin(-radians) * radius + origin.Y;

            return xy;
        }
    }
}