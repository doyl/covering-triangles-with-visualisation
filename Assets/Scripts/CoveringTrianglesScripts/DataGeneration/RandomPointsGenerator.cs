﻿using System;
using System.Collections.Generic;
using System.Numerics;
using CoveringTriangles.Geometry;

namespace CoveringTriangles.DataGeneration
{
    internal class RandomPointsGenerator : IPointsGenerator
    {
        public IEnumerable<Vector2> GeneratePoints(int count)
        {
            float minX = 0;
            float maxX = 100;
            float minY = 0;
            float maxY = 100;
            
            List<Vector2> points = new List<Vector2>();

            for (int i = 0; i < count; ++i)
            {
                points.Add(GeneratePoint(minX, maxX, minY, maxY));
            }

            return points;
        }

        internal Vector2 GeneratePoint(float minX, float maxX, float minY, float maxY)
        {
            //Random rand = new Random();
           //float x = maxX * (float)rand.NextDouble() - minX;
            //float y = maxY * (float) rand.NextDouble() - minY;
            float x = UnityEngine.Random.Range(minX, maxX);
            float y = UnityEngine.Random.Range(minY, maxY);
            return new Vector2(x, y);
        }
    }
}