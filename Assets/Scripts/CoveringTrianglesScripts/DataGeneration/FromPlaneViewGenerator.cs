﻿using System.Collections.Generic;
using System.Drawing;
using System.Numerics;

namespace CoveringTriangles.DataGeneration
{
    public class FromPlaneViewGenerator : IPointsGenerator
    {
        private List<Vector2> points;
        private PlaneView plane;
        public FromPlaneViewGenerator(PlaneView plane)
        {
            this.plane = plane;
        }
        
        public IEnumerable<Vector2> GeneratePoints(int count)
        {
            points  = new List<Vector2>();
            foreach (var dot in plane.dots)
            {
                points.Add(new Vector2(dot.originalPosition.x, dot.originalPosition.y));
            }

            if (points.Count < 3)
            {
                foreach (var p in new RandomPointsGenerator().GeneratePoints(3 - points.Count))
                {
                    points.Add(p);
                }
            }

            return points;
        }
    }
}