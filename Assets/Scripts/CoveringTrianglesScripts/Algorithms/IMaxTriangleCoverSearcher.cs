﻿using CoveringTriangles.Geometry;

namespace CoveringTriangles.Algorithms
{
    internal interface IMaxTriangleCoverSearcher
    {
        Triangle Winning { get; }
        int WinningNumberOfCovers { get; }
        Triangle SearchForPlane(Plane plane);
    }
}