﻿using System;
using System.Collections.Generic;
using CoveringTriangles.Geometry;
using CoveringTriangles.Utilities;
using Plane = CoveringTriangles.Geometry.Plane;

namespace CoveringTriangles.Algorithms
{
    internal class ExpandingTrianglesCoverSearcher : IMaxTriangleCoverSearcher
    {
        private bool[,,] checkMap;
        private int checkedCount;
        public Triangle Winning { get; private set; }
        public int WinningNumberOfCovers { get; private set; }

        private void MarkAsChecked(int i, int j, int k)
        {
            if (checkMap[i, j, k]) return;
            
            checkMap[i, j, k] = true;
            ++checkedCount;
        }
        public Triangle SearchForPlane(Plane plane)
        {
            WinningNumberOfCovers = 0;
            Winning = null;
            List<Point> convexHull = ConvexHull.GetConvexHull(plane.Points);
            int count = plane.Points.Count;
            if (convexHull.Count == count)
            {
                Winning = new Triangle(convexHull[0], convexHull[1], convexHull[2]);
                WinningNumberOfCovers = 3;
                return Winning;
            }
            checkMap = new bool[count,count,count];
            checkedCount = 0;

            CheckForPoints(convexHull, plane);
            //Console.WriteLine("Triangles overall: " + count*(count-1)*(count-2)/6);

            int counter = 0;
            int collinearCounter = 0;
            int alreadyCheckedCounter = 0;

            for (int i = 0; i < count - 2; ++i)
            {
                for (int j = i + 1; j < count - 1; ++j)
                {
                    for (int k = j + 1; k < count; ++k)
                    {
                        if (checkMap[i, j, k])
                        {
                            alreadyCheckedCounter++;
                            continue;
                        }

                        Point p1 = plane.Points[i];
                        Point p2 = plane.Points[j];
                        Point p3 = plane.Points[k];
                        if (PointsCollinearityChecker.ArePointsCollinear(p1, p2, p3))
                        {
                            MarkAsChecked(i, j, k);
                            ++collinearCounter;
                            continue;
                        }
                        ++counter;
                        Triangle toCheck = new Triangle(p1, p2, p3);
                        toCheck = TriangleExpander.Expand(toCheck, plane);
                        
                        List<int> coveredPoints = GetCoveredPoints(toCheck, plane);
                        //Console.WriteLine("Combination of points to delete: " + coveredPoints.Count);
                        DeleteAllCombinations(coveredPoints);
                    }
                }
            }
            
            /*Console.WriteLine("Collinear: " + collinearCounter);
            Console.WriteLine("Triangles compared after expansions: " + counter);
            Console.WriteLine("Checked count = " + checkedCount);
            Console.WriteLine("Already checked counter: " + alreadyCheckedCounter);*/

            return Winning;
        }

        private void CheckForPoints(List<Point> points, Plane plane)
        {
            int count = points.Count;
            bool[,,] checkMap = new bool[count,count,count];
            
            int counter = 0;
            int collinearCounter = 0;
            int alreadyCheckedCounter = 0;

            for (int i = 0; i < count - 2; ++i)
            {
                for (int j = i + 1; j < count - 1; ++j)
                {
                    for (int k = j + 1; k < count; ++k)
                    {
                        Point p1 = points[i];
                        Point p2 = points[j];
                        Point p3 = points[k];
                        if (PointsCollinearityChecker.ArePointsCollinear(p1, p2, p3))
                        {
                            MarkAsChecked(i, j, k);
                            ++collinearCounter;
                            continue;
                        }
                        ++counter;
                        Triangle toCheck = new Triangle(p1, p2, p3);
                        //toCheck = TriangleExpander.Expand(toCheck, plane);
                        
                        List<int> coveredPoints = GetCoveredPoints(toCheck, plane);
                        //Console.WriteLine("Combination of points to delete: " + coveredPoints.Count);
                        DeleteAllCombinations(coveredPoints);
                    }
                }
            }
        }

        private List<int> GetCoveredPoints(Triangle tri, Plane plane)
        {
            List<int> coveredPoints = new List<int>();

            for (int i = 0; i < plane.Points.Count; ++i)
            {
                if (tri.IsCoveringPoint(plane.Points[i].ToVector2()))
                {
                    coveredPoints.Add(i);
                }
            }

            if (coveredPoints.Count > WinningNumberOfCovers)
            {
                WinningNumberOfCovers = coveredPoints.Count;
                Winning = tri;
            }

            return coveredPoints;
        }

        private void DeleteAllCombinations(List<int> points)
        {
            for (int i = 0; i < points.Count-2; ++i)
            {
                for (int j = i+1; j < points.Count-1; ++j)
                {
                    for (int k = j+1; k < points.Count; ++k)
                    {
                        if (!checkMap[points[i], points[j], points[k]])
                        {
                            MarkAsChecked(points[i], points[j], points[k]);
                        }
                    }
                }
            }
        }
    }
}