﻿using System.Collections.Generic;
using System.Numerics;
using CoveringTriangles.Geometry;
using Plane = CoveringTriangles.Geometry.Plane;

namespace CoveringTriangles.Algorithms
{
    internal class BruteForceWithTableSearcher : IMaxTriangleCoverSearcher
    {

        private bool[,,] checkMap;
        private int checkedCount;
        public Triangle Winning { get; private set; }
        public int WinningNumberOfCovers { get; private set; }

        public Triangle SearchForPlane(Plane plane)
        {
            Winning = null;
            WinningNumberOfCovers = 0;
            int count = plane.Points.Count;
            checkMap = new bool[count, count, count];
            checkedCount = 0;

            for (int i = 0; i < count - 2; ++i)
            {
                for (int j = i + 1; j < count - 1; ++j)
                {
                    for (int k = j + 1; k < count; ++k)
                    {
                        if (checkMap[i, j, k])
                        {
                            continue;
                        }
                        
                        Point p1 = plane.Points[i];
                        Point p2 = plane.Points[j];
                        Point p3 = plane.Points[k];
                        if (PointsCollinearityChecker.ArePointsCollinear(p1, p2, p3))
                        {
                            MarkAsChecked(i, j, k);
                            continue;
                        }

                        Triangle toCheck = new Triangle(p1, p2, p3);
                        //toCheck = TriangleExpander.Expand(toCheck, plane);

                        List<int> coveredPoints = GetCoveredPoints(toCheck, plane);
                        //Console.WriteLine("Combination of points to delete: " + coveredPoints.Count);
                        DeleteAllCombinations(coveredPoints);
                    }
                }
            }

            return Winning;
        }

        private List<int> GetCoveredPoints(Triangle tri, Plane plane)
        {
            List<int> coveredPoints = new List<int>();

            for (int i = 0; i < plane.Points.Count; ++i)
            {
                if (tri.IsCoveringPoint(plane.Points[i].ToVector2()))
                {
                    coveredPoints.Add(i);
                }
            }

            if (coveredPoints.Count > WinningNumberOfCovers)
            {
                WinningNumberOfCovers = coveredPoints.Count;
                Winning = tri;
            }

            return coveredPoints;
        }

        private void MarkAsChecked(int i, int j, int k)
        {
            if (checkMap[i, j, k]) return;
            
            checkMap[i, j, k] = true;
            ++checkedCount;
        }
        private void DeleteAllCombinations(List<int> points)
        {
            for (int i = 0; i < points.Count - 2; ++i)
            {
                for (int j = i + 1; j < points.Count - 1; ++j)
                {
                    for (int k = j + 1; k < points.Count; ++k)
                    {
                        if (!checkMap[points[i], points[j], points[k]])
                        {
                            MarkAsChecked(points[i], points[j], points[k]);
                        }
                    }
                }
            }
        }
    }
}