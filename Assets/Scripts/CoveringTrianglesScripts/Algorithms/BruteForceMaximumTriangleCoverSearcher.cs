﻿using System.Collections.Generic;
using System.Numerics;
using CoveringTriangles.Geometry;
using Plane = CoveringTriangles.Geometry.Plane;

namespace CoveringTriangles.Algorithms
{
    internal class BruteForceMaximumTriangleCoverSearcher : IMaxTriangleCoverSearcher
    {
        private List<Triangle> PossibleTriangles { get; set; }
        public Triangle Winning { get; private set; }
        public int WinningNumberOfCovers { get; private set; }

        public Triangle SearchForPlane(Plane plane)
        {
            Winning = null;
            WinningNumberOfCovers = 0;
            plane.CalculateTriangles();
            PossibleTriangles = plane.PossibleTriangles;
            foreach (Triangle tri in PossibleTriangles)
            {
                int numberOfCovers = 0;
                foreach (Point p in plane.Points)
                {
                    if (tri.IsCoveringPoint(new Vector2(p.X, p.Y)))
                        numberOfCovers++;
                }

                if (numberOfCovers > WinningNumberOfCovers)
                {
                    WinningNumberOfCovers = numberOfCovers;
                    Winning = tri;
                }
            }

            return Winning;
        }
    }
}