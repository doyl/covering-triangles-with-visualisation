﻿using System;
using System.Numerics;
using CoveringTriangles.Geometry;

namespace CoveringTriangles.Utilities
{
    internal static class PointsOnTheSameSideChecker
    {
        /// <summary>
        /// Checks if two points are on the same side of line connecting other 2 points
        /// </summary>
        /// <param name="p1">First point to check</param>
        /// <param name="p2">Second point to check</param>
        /// <param name="a">First point of the line</param>
        /// <param name="b">Second point of the line</param>
        /// <returns>True if are on the same side, false otherwise.</returns>
        public static bool IsPointOnSameSide(Vector2 p1, Vector2 p2, Vector2 a, Vector2 b)
        {
            float crossProduct1 = CrossProduct(b - a, p1 - a);
            float crossProduct2 = CrossProduct(b - a, p2 - a);
            //if(Math.Sign(crossProduct1) == Math.Sign(crossProduct2) || crossProduct1 == 0 || crossProduct2 == 0)
            //if (crossProduct1 * crossProduct2 >= 0) 
            if((crossProduct1 >= 0 && crossProduct2 >=0) || (crossProduct1 <=0 && crossProduct2<=0))
                return true;
            return false;
        }

        /// <summary>
        /// Checks if two points are on the same side of line connecting other 2 points
        /// </summary>
        /// <param name="p1">First point to check</param>
        /// <param name="p2">Second point to check</param>
        /// <param name="a">First point of the line</param>
        /// <param name="b">Second point of the line</param>
        /// <returns>True if are on the same side, false otherwise.</returns>
        public static bool IsPointOnSameSide(Point p1, Point p2, Point a, Point b)
        {
            return IsPointOnSameSide(p1.ToVector2(), p2.ToVector2(), a.ToVector2(), b.ToVector2());
        }

        /// <summary>
        /// Returns cross product of given vectors.
        /// </summary>
        /// <param name="first">First vector</param>
        /// <param name="second">Second vector</param>
        /// <returns></returns>
        private static float CrossProduct(Vector2 first, Vector2 second)
        {
            return (first.X * second.Y) - (first.Y * second.X);
        }
    }
}