﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CoveringTriangles
{
    internal static class AlphabeticalNameGenerator
    {
        private static char GetNextLetter(char letter)
        {
            if (letter < 'A' || letter > 'Z')
                throw new Exception("Trying to get next letter for something other than letter! (or small letter)");
            if (letter == 'Z')
                return 'A';

            char next = (char)(letter + 1);
            return next;
        }
        
        public static string GenerateUnique(IEnumerable<string> used)
        {
            if (used.Count() == 0)
                return "A";

            string outputName = "";
            string lastName = used.Last();
            int letterCount = lastName.Length;
            char currentLetter;
            bool incremented = false;
            for (int i = letterCount - 1; i >= 0; --i)
            {
                if (incremented)
                {
                    outputName = lastName[i] + outputName;
                    continue;
                }

                currentLetter = lastName[i];
                if (currentLetter == 'Z')
                {
                    outputName = 'A' + outputName;
                }
                else
                {
                    outputName = GetNextLetter(currentLetter) + outputName;
                    incremented = true;
                }
            }

            if (!incremented)
                outputName = 'A' + outputName;
            return outputName;
        }
    }
}