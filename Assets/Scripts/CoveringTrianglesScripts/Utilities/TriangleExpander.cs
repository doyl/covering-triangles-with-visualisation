﻿using System.Collections.Generic;
using CoveringTriangles.Geometry;

namespace CoveringTriangles.Utilities
{
    internal class TriangleExpander
    {
        public static Triangle Expand(Triangle triangle, Plane plane)
        {
            Point expanded1 = MaxExpandPoint(triangle.Points[0], triangle.Points[1], triangle.Points[2], plane);
            Point expanded2 = MaxExpandPoint(triangle.Points[1], triangle.Points[0], triangle.Points[2], plane);
            Point expanded3 = MaxExpandPoint(triangle.Points[2], triangle.Points[0], triangle.Points[1], plane);
            Triangle expanded = new Triangle(expanded1, expanded2, expanded3);
            return expanded;
        }

        private static Point MaxExpandPoint(Point toExpand, Point p1, Point p2, Plane plane)
        {
            //Console.WriteLine("Max expanding " + toExpand);
            Point oldPoint = toExpand;
            List<Point> possiblePoints = new List<Point>(plane.Points);
            
            while (true)
            {
                Point newPoint = ExpandPoint(oldPoint, p1, p2, possiblePoints);
                if (newPoint == oldPoint)
                {
                    return newPoint;
                }

                oldPoint = newPoint;
            }
        }
        private static Point ExpandPoint(Point toExpand, Point p1, Point p2, List<Point> possiblePoints)
        {
            //Console.WriteLine("Once expanding " + toExpand);
            List<Point> toRemove = new List<Point>();
            Point expanded = toExpand;
            foreach (Point p in possiblePoints)
            {
                if(p == toExpand) continue;
                if(!PointsOnTheSameSideChecker.IsPointOnSameSide(p, p2, toExpand, p1))
                    if (!PointsOnTheSameSideChecker.IsPointOnSameSide(p, p1, toExpand, p2))
                    {
                        //Console.WriteLine("Point " + toExpand + " SURE is EXPANDABLE to point " + p);
                        expanded = p;
                        toRemove.Add(p);
                        break;
                    }

                toRemove.Add(p);
                //Console.WriteLine("Point " + toExpand + " is not expandable to point " + p);
            }

            foreach (Point p in toRemove)
            {
                possiblePoints.Remove(p);
            }

            return expanded;
        }
    }
}