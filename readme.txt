Autor: Olaf Dygas

Specyfikacja problemu:
W układzie współrzędnych XOY znajduje się n punktów. Należy wybrać 3 spośród danych punktów w taki sposób, aby trójkąt o wierzchołkach w tych trzech punktach pokrywał jak największą liczbę punktów.

Aktywacja programu:
Uruchamiamy program w dowolny możliwy w systemie sposób, po uruchomieniu pojawi się domyślne okienko uruchomieniowe silnika Unity pytające o ustawienia wstępne. Pomijamy tę część od razu przyciskając przycisk "Play". Powinno wyświetlić się okienko programu, które udostępni interfejs graficzny do dalszej obsługi.

Dane wejściowe:
Danymi wejściowymi dla algorytmu jest zbiór punktów, które możemy wygenerować za pomocą dostępnych generatorów wybierając jeden z nich z w polu rozwijanym "Generator".
Dostępne generatory to:
-Generator losowy (Random Generator) - generuje on punkty w sposób całkowicie losowy
-Generator wielokątów foremnych (Regular Polygon Generator) - generuje punkty, które są wierzchołkami wielokąta foremnego
-Generator ręczny (Manual Generator) - pozwala użytkownikowi na ręczne wprowadzanie punktów poprzez klikanie prawym przyciskiem myszki na dostępną przestrzeń. Pozwala on także na dodawanie punktów do wcześniej wygenerowanych przez dowolny generator punktów.
Punkty wyświetlane są wewnątrz programu w postaci graficznej na dwuwymiarowej płaszczyźnie.
Przed wygenerowaniem możemy w polu "Points Count" wpisać interesującą nas ilość punktów.

Po wygenerowaniu punktów możemy przystąpić do wyboru algorytmu rozwiązującego nasze zadanie. W polu rozwijanym mamy do wyboru:
-Expanding Triangles - Algorytm wykorzystujący "rozwijanie trójkątów". Rozszerzanie trójkątów jest procesem, w którym poszukujemy na danym zbiorze punktów takich punktów, które połączone ze sobą będą zawierać wewnątrz siebie cały rozszerzany trójkąt. Algorytm omówiony jest bardziej szczegółowo w dalszej części dokumentu.
-Brute Force with deleting - Algorytm będący rozbudowaniem algorytmu brute force o eliminację z dalszego sprawdzania wszystkich trójkątów, które były zawarte w poprzednio sprawdzonych trójkątach. 
-Pure Brute Force - Najprostszy algorytm, który po prostu porównuje ze sobą wszystkie możliwe do uzyskania trójkąty.

Użytkownik w polu "Iterations Count" może zdecydować ile razy chce powtórzyć pomiar danego algorytmu.

Ostatecznie użytkownik może wybrać czy chce aby dla kolejnych powtórzeń pomiarów punkty wejściowe pozostały niezmienione (single generation) lub aby generowały się osobno do każdego pomiaru (keep generating). Do kolejnych generacji używany będzie aktualnie wybrany generator.

Aby rozpocząć pomiary użytkownik powinien nacisnąć przycisk "Calculate".

Prezentacja wyników:
Wynikiem działania pomiaru jest widoczny średni czas, którego algorytm potrzebował na zwrócenie wyniku oraz wizualizacja znalezionego trójkąta na płaszczyźnie, na której wprowadzone były punkty.

Opis algorytmu:

1. Dany jest zbiór punktów Z.
2. Ustalamy zmienne MaxTrójkąt i MaxPunkty = 0. 
3. Tworzymy tablicę zmiennych bool Trójkąty o wymiarach n x n x n, gdzie n to ilość punktów w Z.
4. Wybieramy dowolny trójkąt ABC złożony z punktów w Z, które nie są oznaczone w Trójkąty jako wyeliminowane.
5. Maksymalnie rozszerzamy trójkąt ABC w trójkąt A’B’C’.
6. Przypisujemy do W ilość punktów zawartą w trójkącie A’B’C’.
7. Jeśli W jest większe od MaxPunkty to przypisujemy 
8. MaxPunkty := W i MaxTrójkąt := A’B’C’.
9. Eliminujemy wszystkie trójkąty zawarte w A’B’C’.
10. Jeśli istnieją jeszcze niewyeliminowane trójkąty to wracamy do punktu 5.
11. Zwracamy trójkąt MaxTrójkąt.

Przewodnik po plikach źródłowych:
Pliki odpowiedzialne za algorytm znajdują się w folderze Assets/Scripts/CoveringTrianglesScripts. Pozostałe pliki źródłowe (CameraDrag.cs, DotView.cs, Initializer.cs, PlaneView.cs, PointsAdder.cs) zostały stworzone na potrzeby wizualizacji i nie uczestniczą bezpośrednio w algorytmie.

Wewnątrz folderu CoveringTrianglesScripts znajdują się 4 foldery:
Algorithms - zawiera implementacje algorytmów (BruteForceMaximumTriangleCoverSearcher.cs, BruteForceWithTableCoverSearcher.cs, ExpandingTrianglesCoverSearcher.cs) oraz interfejs, który algorytmy implementują (IMaxTriangleCoverSearcher.cs).
DataGeneration - zawiera generatory punktów (FromPlaneViewGenerator.cs, RandomPointsGenerator.cs, RegularPolygonGenerator.cs) oraz interfejs, który te generatory implementują (IPointsGenerator.cs)
Geometry - zawiera struktury geometryczne użyte w badaniu algorytmów (ConvexHull.cs, Plane.cs, Point.cs, Triangle.cs)
Utilities - zawiera pomocne funkcje wykorzystywane w programie (AlphabeticalNameGenerator.cs, PointsOnTheSameSideChecker.cs, TriangleCoveringPointsChecker.cs, TriangleExpander.cs).